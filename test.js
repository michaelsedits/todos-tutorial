var request = require('supertest');
var app = require('./app');

var redis = require('redis');
var client = redis.createClient();
client.select('test'.length);
client.flushdb();

describe('Requests to the root path', function() {
  
  it('Returns a 200 status code', function(done) {
    
  request(app)
    .get('/')
    .expect(200, done);
    
  });
  
  it('Returns an HTML format', function(done) {
    
    request(app)
      .get('/')
      .expect('Content-Type', /html/, done);
    
  });
  
  it('Returns an index file with Todos', function(done) {
    
    request(app)
      .get('/')
      .expect(/todos/i, done);
    
  });
  
});

describe('Listing to-dos on /todos', function(){
  
  it('Returns 200 status code', function(done){
      
      request(app)
        .get('/todos')
        .expect(200, done);
  });
  
  it('Returns JSON format', function(done) {
    
    request(app)
      .get('/todos')
      .expect('Content-Type', /json/, done);
  
  });
  
  it ('Returns initial to-dos', function(done) {
    
    request(app)
      .get('/todos')
    .expect(JSON.stringify([]), done);
    
  });
  
});

describe('Creating new todos', function(){
  it('Returns a 201 status code', function(done){
    request(app)
      .post('/todos')
      .send('name=Exercise&description=Run+fast')
      .expect(201, done);
  });
  
  it('Returns the to-do name', function(done){
    request(app)
      .post('/todos')
      .send('name=Exercise&description=Run+fast')
      .expect(/exercise/i, done);
  });
  
  it('Validates to-do name and description', function(done) {
    request(app)
      .post('/todos')
      .send('name=&description=')
      .expect(400, done)
  });
});

describe('Deleting to-dos', function(){
  
  before(function(){
    client.hset('todos', 'Run', 'Four miles');
  });
  
  after(function(){
    client.flushdb();
  });
  
  it('Returns a 204 status code', function(done){
    request(app)
      .delete('/todos/Run')
      .expect(204)
      .end(function(error){
        if(error) throw error;
        done();
      });
  });
});

describe('Shows to-do info', function(){
  
  before(function() {
    client.hset('todos', 'Run', 'Four');
  });
  
  after(function() {
    client.flushdb();
  });
  
  it('Returns 200 status code', function(done){
    request(app)
      .get('/todos/Run')
      .expect(200, done);
  });
  
  it('Returns HTML format', function(done){
    request(app)
      .get('/todos/Run')
      .expect('Content-Type', /html/, done)
  });
})